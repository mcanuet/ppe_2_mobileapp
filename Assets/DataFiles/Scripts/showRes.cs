﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using Proyecto26;
using UnityEngine.SceneManagement;
using apiModels;
using System;

public class showRes : MonoBehaviour
{
    //UI control
    [SerializeField]
    private Text Number;
    [SerializeField]
    private Text StartDate;
    [SerializeField]
    private Text EndDate;
    [SerializeField]
    private Text Plate;
    [SerializeField]
    private Text Car;

    [SerializeField]
    private Image loadingPanel;
    [SerializeField]
    private Image spinner;

    private RequestHelper currentRequest;
    private Reservation[] AllRes;

    private int Position;

    /// <summary>
    /// appelé lors de la première execution
    /// </summary>
    void Start()
    {
        loadingPanel.enabled = spinner.enabled = false;
        GetRes();
    }

    /// <summary>
    /// appelè à toutes les frames et permet à l'utilisateur de retourner au menu principale.
    /// </summary>
    void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            SceneManager.LoadScene(2);
        }
    }

    /// <summary>
    /// récupère toutes les réservations de l'utilisateur.
    /// </summary>
    private void GetRes()
    {
        Position = 0;
        string today = DateTime.Today.ToString("yyyy-MM-dd");
        loadingPanel.enabled = spinner.enabled = true;
        currentRequest = new RequestHelper
        {
            Uri = PlayerPrefs.GetString("baseUrl") + "reservation?user_id=" + PlayerPrefs.GetInt("userID") + "&endgt=" + today,
            Headers = new Dictionary<string, string> {
                    { "token", PlayerPrefs.GetString("token") }
                },
        };
        RestClient.GetArray<Reservation>(currentRequest)
        .Then(res => { AllRes = res; MakeText(); })
        .Catch(err => Debug.Log(err));
    }

    /// <summary>
    /// créer les textes informatifes
    /// </summary>
    private void MakeText()
    {
        loadingPanel.enabled = spinner.enabled = true;
        if (AllRes.Length == 1)
        {
            Plate.text = "Aucune réservation";
        } else {
            Reservation reservation = AllRes[Position];
            StartDate.text = "Début: " + reservation.start;
            EndDate.text = "Fin: " + reservation.end;
            Number.text = reservation.id.ToString();
            Plate.text = reservation.car.plate_number;
            Car.text = reservation.car.model.brand + " " + reservation.car.model.model;
        }
            loadingPanel.enabled = spinner.enabled = false;
    }

    /// <summary>
    /// récupère la reservation suivante et modifie les textes
    /// </summary>
    public void GoRight()
    {
        if (Position + 1 < AllRes.Length - 1 )
            Position++;
        MakeText();
    }

    /// <summary>
    /// récupère la reservation précédente et modifie les textes
    /// </summary>
    public void GoLeft()
    {
        if (Position - 1 >= 0)
            Position--;
        MakeText();
    }

    /// <summary>
    /// annule une réservation
    /// </summary>
    public void CancelReservation()
    {
        loadingPanel.enabled = spinner.enabled = true;
        currentRequest = new RequestHelper
        {
            Uri = PlayerPrefs.GetString("baseUrl") + "reservation?id=" + AllRes[Position].id,
            Headers = new Dictionary<string, string> {
                    { "token", PlayerPrefs.GetString("token") }
                }
        };
        RestClient.Delete(currentRequest)
            .Then(res => GetRes())
            .Catch(err => Debug.Log(err));
    }
}
