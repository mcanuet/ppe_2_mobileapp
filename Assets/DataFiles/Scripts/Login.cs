﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Proyecto26;
using apiModels;

public class Login : MonoBehaviour
{
    public Text emailField;
    public InputField passwordField;
    public Text failText;
    public Image spinner;
    public Image failImage;

    private RequestHelper currentRequest;

    /// <summary>
    /// appelé lors de la première execution
    /// </summary>
    private void Start()
    {
        failText.enabled = false;
        failImage.enabled = false;
        spinner.enabled = false;
        if (PlayerPrefs.GetString("baseUrl") == null)
            PlayerPrefs.SetString("baseUrl", "http://sio.monparcoursenligne.fr/mcanuet/ppe_2_backend/index.php/");
        if (PlayerPrefs.GetString("token") != "" && PlayerPrefs.GetString("userID") != "")
        {
            SceneManager.LoadScene(2);
        }
    }

    /// <summary>
    /// appelè à toutes les frames et permet à l'utilisateur de quiter l'application.
    /// </summary>
    private void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    /// <summary>
    /// activer lors de l'envoie des informations de connexion.
    /// </summary>
    public void onPressLogin()
    {
        var mail = emailField.text.ToString();
        var pwd = passwordField.text.ToString();
        failText.enabled = false;
        spinner.enabled = true;
        failImage.enabled = false;
        GetToken(mail, pwd);
    }

    /// <summary>
    /// permet la récupération du token de l'utilisateur
    /// </summary>
    /// <param name="email">email de l'utilisateur</param>
    /// <param name="password">mot de passe de l'utilisateur</param>
    public void GetToken(string email , string password)
    {
        currentRequest = new RequestHelper
        {
            Uri = PlayerPrefs.GetString("baseUrl") + "user/login" ,
            Body = new User
            {
                email = email,
                password = password
            }
        };
        RestClient.Post<LoginData>(currentRequest)
        .Then(res  => LoginSuccess(res.token, res.self.user_id))
        .Catch(err => FailedLogin());
    }

    /// <summary>
    /// utiliser si l'authentification réussi, charge la scene de tableau de bord
    /// </summary>
    /// <param name="token">token de l'utilisateur</param>
    /// <param name="userID">identifiant de l'utilisateur</param>
    private void LoginSuccess(string token, int userID)
    {
        PlayerPrefs.SetString("token", token);
        PlayerPrefs.SetInt("userID", userID);
        PlayerPrefs.Save();
        spinner.enabled = false;
        SceneManager.LoadScene(2);
    }

    /// <summary>
    /// utiliser si l'authentification ne réussie pas
    /// </summary>
    private void FailedLogin()
    {
        spinner.enabled = false;
        failText.enabled = true;
        failImage.enabled = true;
    }

    /// <summary>
    /// charge le scène pour les paramètres
    /// </summary>
    public void OnSettingsPress()
    {
        SceneManager.LoadScene(6);
    }
}