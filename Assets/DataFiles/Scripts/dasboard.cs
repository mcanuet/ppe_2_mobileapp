﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class dasboard : MonoBehaviour
{
    /// <summary>
    /// fonction appelé toutes les frames
    /// </summary>
    private void Update()
    {
        //détecte l'appuie sur le bouton retour d'android et quite l'application
        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    /// <summary>
    /// permet la deconnexion de l'utilisateur de l'application
    /// </summary>
    public void OnDecoPress()
    {
        PlayerPrefs.DeleteKey("token");
        SceneManager.LoadScene(0);
    }

    /// <summary>
    /// charge la scène des nouvelle réservations
    /// </summary>
    public void OnNewResPress()
    {
        SceneManager.LoadScene(3);
    }

    /// <summary>
    /// charge la scène de présentation des reservation
    /// </summary>
    public void OnShowResPress()
    {
        SceneManager.LoadScene(4);
    }

    /// <summary>
    /// charge la scène de présentation du model 3D
    /// </summary>
    public void OnShowModelPress()
    {
        SceneManager.LoadScene(1);
    }

    /// <summary>
    /// charge le scène pour les paramètres
    /// </summary>
    public void OnSettingsPress()
    {
        SceneManager.LoadScene(6);
    }
}
