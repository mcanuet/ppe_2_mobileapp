﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using Proyecto26;
using UnityEngine.SceneManagement;
using apiModels;

public class makeRes : MonoBehaviour
{
    // model and brand choice
    [SerializeField]
    private Dropdown brands;
    [SerializeField]
    private Dropdown models;

    //duration choice
    //start
    public Dropdown startDay;
    public Dropdown startMonth;
    public Dropdown startHour;
    public Dropdown startMinute;

    //end
    public Dropdown endDay;
    public Dropdown endMonth;
    public Dropdown endHour;
    public Dropdown endMinute;

    // loading control
    [SerializeField]
    private Image loadingPanel;
    [SerializeField]
    private Image spinner;

    // modal control
    [SerializeField]
    private GameObject modal;
    [SerializeField]
    private Text modalInfo;

    private Model[] AllModels;
    private Model[] AllModelsByBrand;
    private Model SelectModel;
    private RequestHelper currentRequest;
    private String[] months = { "janvier", "fevrier", "mars", "avril", "mai", "juin", "juillet", "aout", "septembre", "octobre", "novembre", "décembre" };

    /// <summary>
    /// appelé lors de la première execution
    /// </summary>
    void Start()
    {
        modal.SetActive(false);
        loadingPanel.enabled = spinner.enabled = false;
        GetModel();
        MakeDateDropdown();
    }

    /// <summary>
    /// appelè à toutes les frames et permet à l'utilisateur de retourner au menu principale.
    /// </summary>
    private void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            BackToDashboard();
        }
    }

    /// <summary>
    /// detecte le choix de la marque et change les models disponibles
    /// </summary>
    public void OnBrandValueChanged()
    {
        GetModel(brands.captionText.text);
    }

    /// <summary>
    /// récupère les models en fonction d'une marque
    /// </summary>
    /// <param name="brand">marque de la voiture</param>
    private void GetModel(string brand = "")
    {
        loadingPanel.enabled = spinner.enabled = true;
        if (brand != "")
            brand ="?brand=" + brand;
        currentRequest = new RequestHelper
        {
            Uri = PlayerPrefs.GetString("baseUrl") + "model" + brand,
            Headers = new Dictionary<string, string> {
                    { "token", PlayerPrefs.GetString("token") }
                },
        };
        RestClient.GetArray<Model>(currentRequest)
        .Then(res => {
            if (brand == "")
            {
                AllModels = res;
                MakeDropdownBrandOptions();
                GetModel(res[0].brand);
            } else
            {
                AllModelsByBrand = res;
                MakeDropdownModelOptions();
            }
        });
    }

    /// <summary>
    /// creér les éléments de la liste d'éroulante des marques
    /// </summary>
    private void MakeDropdownBrandOptions()
    {
        brands.ClearOptions();
        List<String> displayed = new List<string>();
        foreach (Model brand in AllModels)
        {
            if (!displayed.Contains(brand.brand))
            {
                brands.options.Add(new Dropdown.OptionData(brand.brand));
                displayed.Add(brand.brand);
            }
        }
        brands.captionText.text = brands.options[0].text;
        loadingPanel.enabled = spinner.enabled = false;
    }

    /// <summary>
    /// creér les éléments de la liste d'éroulante des models
    /// </summary>
    private void MakeDropdownModelOptions()
    {
        models.ClearOptions();
        foreach (Model model in AllModelsByBrand)
        {
            models.options.Add(new Dropdown.OptionData(model.model));
        }
        models.captionText.text = models.options[0].text;
        loadingPanel.enabled = spinner.enabled = false;
    }

    /// <summary>
    /// creér les éléments de la liste d'éroulante des dates
    /// </summary>
    private void MakeDateDropdown()
    {
        loadingPanel.enabled = true;
        spinner.enabled = true;

        for (int i = 1; i <= 31; i++)
        {
            startDay.options.Add(new Dropdown.OptionData(i.ToString()));
            endDay.options.Add(new Dropdown.OptionData(i.ToString()));
        }

        foreach (String month in months)
        {
            startMonth.options.Add(new Dropdown.OptionData(month));
            endMonth.options.Add(new Dropdown.OptionData(month));
        }

        for (int i = 0; i < 24; i++)
        {
            String H = (i < 10) ? "0" + i.ToString() : i.ToString();
            startHour.options.Add(new Dropdown.OptionData(H));
            endHour.options.Add(new Dropdown.OptionData(H));
        }

        for (int i = 0; i < 61; i++)
        {
            String M = (i < 10) ? "0" + i.ToString() : i.ToString();
            startMinute.options.Add(new Dropdown.OptionData(M));
            endMinute.options.Add(new Dropdown.OptionData(M));
        }
        startDay.captionText.text    = startDay.options[0].text;
        startMonth.captionText.text  = startMonth.options[0].text;
        startHour.captionText.text   = startHour.options[0].text;
        startMinute.captionText.text = startMinute.options[0].text;

        endDay.captionText.text    = endDay.options[0].text;
        endMonth.captionText.text  = endMonth.options[0].text;
        endHour.captionText.text   = endHour.options[0].text;
        endMinute.captionText.text = endMinute.options[0].text;

        spinner.enabled = false;
        loadingPanel.enabled = false;
    }

    /// <summary>
    /// envoi la nouvelle réservation
    /// </summary>
    public void SendRes()
    {
        spinner.enabled = true;
        loadingPanel.enabled = true;

        currentRequest = new RequestHelper
        {
            Uri = PlayerPrefs.GetString("baseUrl") + "model?model=" + models.captionText.text + "&brand=" + brands.captionText.text,
            Headers = new Dictionary<string, string> {
                    { "token", PlayerPrefs.GetString("token") }
                },
        };
        RestClient.GetArray<Model>(currentRequest)
        .Then(res => {
            int modelID = res[0].id;
            if (modelID == 0)
            {
                spinner.enabled = false;
                loadingPanel.enabled = false;
                return;
            }
            DateTime today = DateTime.Now;
            //String start = new DateTime( today.Year, Array.IndexOf(months, startMonth.captionText.text) + 1, Int32.Parse(startDay.captionText.text), Int32.Parse(startHour.captionText.text), Int32.Parse(startMinute.captionText.text), 0).ToString().Replace('/', '-');
            //String end = new DateTime( today.Year, Array.IndexOf(months, endMonth.captionText.text) + 1, Int32.Parse(endDay.captionText.text), Int32.Parse(endHour.captionText.text), Int32.Parse(endMinute.captionText.text), 0).ToString().Replace('/', '-');
            String start = today.Year.ToString() + "-" + (Array.IndexOf(months, startMonth.captionText.text) + 1).ToString() + "-" + startDay.captionText.text + " " + startHour.captionText.text + ":" + startMinute.captionText.text + ":00";
            String end   = today.Year.ToString() + "-" + (Array.IndexOf(months, endMonth.captionText.text) + 1).ToString() + "-" + endDay.captionText.text + " " + endHour.captionText.text + ":" + endMinute.captionText.text + ":00";
            Debug.Log(start);
            currentRequest = new RequestHelper
            {
                Uri = PlayerPrefs.GetString("baseUrl") + "model/makeReservation",
                Body = new MakeReservation
                {
                    user_id = PlayerPrefs.GetInt("userID"),
                    model_id = modelID,
                    start = start,
                    end = end
                }
            };
            RestClient.Post<Reservation>(currentRequest)
            .Then(finalRes => OpenModal("La réservation est un succés"))
            .Catch(err => OpenModal("Echec de la reservation \n" + err.Message ));
        });

        spinner.enabled = false;
        loadingPanel.enabled = false;

    }

    /// <summary>
    /// ouvre une fenêtre idiquant l'état de le reqêtte
    /// </summary>
    /// <param name="msg">message à afficher</param>
    private void OpenModal(string msg)
    {
        modalInfo.text = msg;
        modal.SetActive(true);
    }

    /// <summary>
    /// ferme la fenêtre informative
    /// </summary>
    public void CloseModale()
    {
        modalInfo.text = "";
        modal.SetActive(false);
    }

    /// <summary>
    /// charge la scéne de tableau de bord
    /// </summary>
    public void BackToDashboard()
    {
        SceneManager.LoadScene(2);
    }
}
