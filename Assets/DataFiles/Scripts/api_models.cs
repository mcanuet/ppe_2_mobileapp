﻿using System;

/// <summary>
/// description des objet reçu ou nvenyé à l'API.
/// </summary>
namespace apiModels
{
    [Serializable]
    public class LoginData
    {
        public string token;
        public Self self;
    }

    [Serializable]
    public class Self
    {
        public int user_id;
        public string email;
        public string phone;
        public string zip_code;
        public string town;
        public string street_name;
        public int street_number;
        public string group;
    }

    [Serializable]
    public class Right
    {
        public int id;
        public string methode;
        public string table;
    }

    [Serializable]
    public class Group
    {
        public int id;
        public string name;
        public Right[] rights;
    }

    [Serializable]
    public class User
    {
        public int id;
        public string email;
        public string password;
        public string phone;
        public string zip_code;
        public string town;
        public string street_name;
        public int street_number;
        public Group group;
    }

    [Serializable]
    public class Model
    {
        public int id;
        public string brand;
        public string model;
        public string fuel_type;
        public string image;
        public int places;
        public int engine;
        public string category;
    }

    [Serializable]
    public class Car
    {
        public int id;
        public string plate_number;
        public int kilometers_count;
        public String last_control;
        public string ref_insurance;
        public Boolean disponibilty;
        public string status;
        public Model model;
    }

    [Serializable]
    public class Reservation
    {
        public int id;
        public String start;
        public String end;
        public User user;
        public Car car;
    }

    [Serializable]
    public class MakeReservation
    {
        public int user_id;
        public int model_id;
        public String start;
        public String end;
    }
}