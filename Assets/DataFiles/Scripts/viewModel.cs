﻿using UnityEngine;
using UnityEngine.UI;
using Proyecto26;
using apiModels;
using System;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class viewModel : MonoBehaviour
{
    // model and brand choice
    [SerializeField]
    private Dropdown brands;
    [SerializeField]
    private Dropdown models;

    // loading control
    [SerializeField]
    private Image loadingPanel;
    [SerializeField]
    private Image spinner;

    private Model[] AllModels;
    private Model[] AllModelsByBrand;
    private Model SelectModel;
    private RequestHelper currentRequest;

    // Start is called before the first frame update
    void Start()
    {
        loadingPanel.enabled = spinner.enabled = false;
        GetModel();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            BackToDashboard();
        }
    }

    public void OnBrandValueChanged()
    {
        GetModel(brands.captionText.text);
    }

    private void GetModel(string brand = "")
    {
        loadingPanel.enabled = spinner.enabled = true;
        if (brand != "")
            brand = "?brand=" + brand;
        currentRequest = new RequestHelper
        {
            Uri = PlayerPrefs.GetString("baseUrl") + "model" + brand,
            Headers = new Dictionary<string, string> {
                    { "token", PlayerPrefs.GetString("token") }
                },
        };
        RestClient.GetArray<Model>(currentRequest)
        .Then(res => {
            if (brand == "")
            {
                AllModels = res;
                MakeDropdownBrandOptions();
                GetModel(res[0].brand);
            }
            else
            {
                AllModelsByBrand = res;
                MakeDropdownModelOptions();
            }
        });
    }

    private void MakeDropdownBrandOptions()
    {
        loadingPanel.enabled = spinner.enabled = true;
        brands.ClearOptions();
        List<String> displayed = new List<string>();
        foreach (Model brand in AllModels)
        {
            if (!displayed.Contains(brand.brand))
            {
                brands.options.Add(new Dropdown.OptionData(brand.brand));
                displayed.Add(brand.brand);
            }
        }
        brands.captionText.text = brands.options[0].text;
        loadingPanel.enabled = spinner.enabled = false;
    }

    private void MakeDropdownModelOptions()
    {
        models.ClearOptions();
        foreach (Model model in AllModelsByBrand)
        {
            models.options.Add(new Dropdown.OptionData(model.model));
        }
        models.captionText.text = models.options[0].text;
        loadingPanel.enabled = spinner.enabled = false;
    }

    private void BackToDashboard()
    {
        SceneManager.LoadScene(2);
    }

    public void GoToModelViewer()
    {
        SceneManager.LoadScene(1);
    }
}
