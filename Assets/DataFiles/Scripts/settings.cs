﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class settings : MonoBehaviour
{
    [SerializeField]
    private InputField backendUrl;

    // Start is called before the first frame update
    void Start()
    {
        backendUrl.text = PlayerPrefs.GetString("baseUrl");
    }

    /// <summary>
    /// appelè à toutes les frames et permet à l'utilisateur de quiter l'application.
    /// </summary>
    private void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            SceneManager.LoadScene(0);
        }
    }

    public void OnSendPress()
    {
        Debug.Log(backendUrl.text.ToString());
        PlayerPrefs.DeleteKey("baseUrl");
        PlayerPrefs.SetString("baseUrl", backendUrl.text.ToString());
        PlayerPrefs.Save();
    }
}
