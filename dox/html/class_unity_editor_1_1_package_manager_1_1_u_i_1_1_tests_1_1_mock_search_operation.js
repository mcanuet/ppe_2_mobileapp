var class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_mock_search_operation =
[
    [ "MockSearchOperation", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_mock_search_operation.html#a1e3de6863ff77b815516cb10a818bd1c", null ],
    [ "GetAllPackageAsync", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_mock_search_operation.html#add03cecd70ccc2ab64efe6787c5333a6", null ],
    [ "Packages", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_mock_search_operation.html#a1e671eae830da11be4dd524642a966ee", null ],
    [ "OnOperationError", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_mock_search_operation.html#ac8ac897b205580ab4e533d8e24f3525f", null ],
    [ "OnOperationFinalized", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_mock_search_operation.html#a99c3db8fba2c1ac12e115cc20746c0c8", null ]
];