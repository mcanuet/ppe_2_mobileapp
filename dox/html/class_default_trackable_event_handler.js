var class_default_trackable_event_handler =
[
    [ "OnDestroy", "class_default_trackable_event_handler.html#af7e768a74ee9c5062694df35c938e037", null ],
    [ "OnTrackableStateChanged", "class_default_trackable_event_handler.html#a416b73a3932ace040a857caf480b4a27", null ],
    [ "OnTrackingFound", "class_default_trackable_event_handler.html#ac49b00961e889974eb8f987c79cdf7ad", null ],
    [ "OnTrackingLost", "class_default_trackable_event_handler.html#a9dd05475b15a2d80bcd5cb7888fe93ac", null ],
    [ "Start", "class_default_trackable_event_handler.html#aeaf8827b3cf01a95736cdcc8f0af8a4e", null ],
    [ "m_NewStatus", "class_default_trackable_event_handler.html#a01c27c83e25b977eac8505074589439a", null ],
    [ "m_PreviousStatus", "class_default_trackable_event_handler.html#a79fd2db8356387ac6d4ee5ba638e54c1", null ],
    [ "mTrackableBehaviour", "class_default_trackable_event_handler.html#a733ff725b85cb2ab0daa6c43718942f1", null ]
];