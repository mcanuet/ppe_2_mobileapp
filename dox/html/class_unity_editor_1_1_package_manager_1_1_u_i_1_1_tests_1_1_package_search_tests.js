var class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_search_tests =
[
    [ "MatchCriteria_CriteriaDoesntMatch_ReturnsFalse", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_search_tests.html#ad24487967c023724fa4fc16c5ec346a6", null ],
    [ "MatchCriteria_CriteriaMatchCurrentVersion_ReturnsTrue", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_search_tests.html#a2ba0175162fd71f13e527c8ffd3d4d8e", null ],
    [ "MatchCriteria_CriteriaMatchCurrentVersionPreRelease_ReturnsTrue", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_search_tests.html#af4d61f55af7634bf9b20bf598272c73e", null ],
    [ "MatchCriteria_CriteriaMatchDisplayNamePartially_ReturnsTrue", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_search_tests.html#ab7d9d52f26706b43529d74fd315c10b9", null ],
    [ "MatchCriteria_CriteriaPartialMatchCurrentVersionPreRelease_ReturnsTrue", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_search_tests.html#ad7764ccdca43f1c514b61de7e4bf494a", null ],
    [ "MatchCriteria_CriteriaPartialMatchCurrentVersionPreReleaseLeadingDash_ReturnsTrue", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_search_tests.html#a8a65a57c2927b54bbf286f9096233450", null ],
    [ "MatchCriteria_CriteriaPartialMatchVerified_ReturnsTrue", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_search_tests.html#a1abc0ff8b9f82ebc843c92a0c3e88e92", null ],
    [ "MatchCriteria_NullOrEmptyCriteria_ReturnsTrue", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_search_tests.html#a09426b3f6170cb8d9a1afbdcfb38842c", null ],
    [ "Setup", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_search_tests.html#ae0af173d00abe16f5507a58042c4f4c7", null ]
];