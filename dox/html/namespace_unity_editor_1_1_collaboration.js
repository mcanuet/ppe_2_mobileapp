var namespace_unity_editor_1_1_collaboration =
[
    [ "Tests", "namespace_unity_editor_1_1_collaboration_1_1_tests.html", "namespace_unity_editor_1_1_collaboration_1_1_tests" ],
    [ "BuildStatusButton", "class_unity_editor_1_1_collaboration_1_1_build_status_button.html", "class_unity_editor_1_1_collaboration_1_1_build_status_button" ],
    [ "CollabHistoryDropDown", "class_unity_editor_1_1_collaboration_1_1_collab_history_drop_down.html", "class_unity_editor_1_1_collaboration_1_1_collab_history_drop_down" ],
    [ "CollabHistoryDropDownItem", "class_unity_editor_1_1_collaboration_1_1_collab_history_drop_down_item.html", "class_unity_editor_1_1_collaboration_1_1_collab_history_drop_down_item" ],
    [ "CollabHistoryItem", "class_unity_editor_1_1_collaboration_1_1_collab_history_item.html", "class_unity_editor_1_1_collaboration_1_1_collab_history_item" ],
    [ "CollabHistoryItemFactory", "class_unity_editor_1_1_collaboration_1_1_collab_history_item_factory.html", "class_unity_editor_1_1_collaboration_1_1_collab_history_item_factory" ],
    [ "CollabHistoryPresenter", "class_unity_editor_1_1_collaboration_1_1_collab_history_presenter.html", "class_unity_editor_1_1_collaboration_1_1_collab_history_presenter" ],
    [ "CollabHistoryRevisionLine", "class_unity_editor_1_1_collaboration_1_1_collab_history_revision_line.html", "class_unity_editor_1_1_collaboration_1_1_collab_history_revision_line" ],
    [ "HistoryProgressSpinner", "class_unity_editor_1_1_collaboration_1_1_history_progress_spinner.html", "class_unity_editor_1_1_collaboration_1_1_history_progress_spinner" ],
    [ "ICollabHistoryItemFactory", "interface_unity_editor_1_1_collaboration_1_1_i_collab_history_item_factory.html", "interface_unity_editor_1_1_collaboration_1_1_i_collab_history_item_factory" ],
    [ "IPagerData", "interface_unity_editor_1_1_collaboration_1_1_i_pager_data.html", "interface_unity_editor_1_1_collaboration_1_1_i_pager_data" ],
    [ "PagedListView", "class_unity_editor_1_1_collaboration_1_1_paged_list_view.html", "class_unity_editor_1_1_collaboration_1_1_paged_list_view" ],
    [ "PagerElement", "class_unity_editor_1_1_collaboration_1_1_pager_element.html", "class_unity_editor_1_1_collaboration_1_1_pager_element" ],
    [ "StatusView", "class_unity_editor_1_1_collaboration_1_1_status_view.html", "class_unity_editor_1_1_collaboration_1_1_status_view" ]
];