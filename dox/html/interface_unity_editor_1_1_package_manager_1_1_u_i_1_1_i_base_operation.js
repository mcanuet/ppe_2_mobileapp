var interface_unity_editor_1_1_package_manager_1_1_u_i_1_1_i_base_operation =
[
    [ "Cancel", "interface_unity_editor_1_1_package_manager_1_1_u_i_1_1_i_base_operation.html#ab924259fe1ff324c238ae37c96c42c1e", null ],
    [ "IsCompleted", "interface_unity_editor_1_1_package_manager_1_1_u_i_1_1_i_base_operation.html#afd5cd3a0d72355105f5ba5e6ad373d1a", null ],
    [ "OnOperationError", "interface_unity_editor_1_1_package_manager_1_1_u_i_1_1_i_base_operation.html#a59bdb665b62e88e5fbbcd0d6e0c9d840", null ],
    [ "OnOperationFinalized", "interface_unity_editor_1_1_package_manager_1_1_u_i_1_1_i_base_operation.html#a06c7f83f9ae4c86c58022b867d670e60", null ]
];