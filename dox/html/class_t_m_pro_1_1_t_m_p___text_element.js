var class_t_m_pro_1_1_t_m_p___text_element =
[
    [ "height", "class_t_m_pro_1_1_t_m_p___text_element.html#a8e0536e8531dafd5cd8737d0c4fb28bc", null ],
    [ "id", "class_t_m_pro_1_1_t_m_p___text_element.html#ad6929a466e937d3db703223dfe1e66c7", null ],
    [ "scale", "class_t_m_pro_1_1_t_m_p___text_element.html#a555701c73d767af5770e029ef3b243b2", null ],
    [ "width", "class_t_m_pro_1_1_t_m_p___text_element.html#ae65ca9c0aba4731278f18a8c4890628b", null ],
    [ "x", "class_t_m_pro_1_1_t_m_p___text_element.html#a584627e5824a29fbd54c2606798cc433", null ],
    [ "xAdvance", "class_t_m_pro_1_1_t_m_p___text_element.html#aaf605eba3a287ec2c4f993790e444e72", null ],
    [ "xOffset", "class_t_m_pro_1_1_t_m_p___text_element.html#ad7d4c5cd8e004c37301cebea4ccef2f1", null ],
    [ "y", "class_t_m_pro_1_1_t_m_p___text_element.html#a8e2cedf1184993e8d566bc0b95b8532e", null ],
    [ "yOffset", "class_t_m_pro_1_1_t_m_p___text_element.html#a69b38f4991fe27916600dc332326ec80", null ]
];