var class_semver_1_1_sem_version =
[
    [ "SemVersion", "class_semver_1_1_sem_version.html#aa0c5f4dbdd36760ada7bd67a65030f7a", null ],
    [ "SemVersion", "class_semver_1_1_sem_version.html#aded2959db05983a3041020acf415eecb", null ],
    [ "Change", "class_semver_1_1_sem_version.html#a5d3a10589ea96a59aed64d65ab5f9c24", null ],
    [ "CompareByPrecedence", "class_semver_1_1_sem_version.html#adabe1c3ea77ec1dae140cdc3aa4c60b1", null ],
    [ "CompareTo", "class_semver_1_1_sem_version.html#aef2d58904fc4226c0f4688de353e8204", null ],
    [ "CompareTo", "class_semver_1_1_sem_version.html#ad72682b72071f1a8fef88c7a978db04c", null ],
    [ "Equals", "class_semver_1_1_sem_version.html#a32a7224158056561d19ee7b2973f4ce1", null ],
    [ "GetHashCode", "class_semver_1_1_sem_version.html#ac570f6109ec0a58cbe5127b7505c654d", null ],
    [ "GetObjectData", "class_semver_1_1_sem_version.html#ab7a3c2f630b6220ecb7c05c21aea9eb5", null ],
    [ "PrecedenceMatches", "class_semver_1_1_sem_version.html#a8a4c9fdc124a640e26a9d81e6b6ea257", null ],
    [ "ToString", "class_semver_1_1_sem_version.html#a6a2cd954a8993437f925b94d1c54b59c", null ],
    [ "Build", "class_semver_1_1_sem_version.html#a665a136e947cb428968ebe21579ac0cf", null ],
    [ "Major", "class_semver_1_1_sem_version.html#a11294c417e3952d0429cbc7e398d0d49", null ],
    [ "Minor", "class_semver_1_1_sem_version.html#a74229ba692723ce4079715c3cff26413", null ],
    [ "Patch", "class_semver_1_1_sem_version.html#a03f77dd74e47eefff519ba00cd43db9a", null ],
    [ "Prerelease", "class_semver_1_1_sem_version.html#a8bb1554a68d74cef681af7cc01d460ef", null ]
];