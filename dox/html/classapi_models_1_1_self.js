var classapi_models_1_1_self =
[
    [ "email", "classapi_models_1_1_self.html#a160d91f97d54429fa0ca3692a87668b3", null ],
    [ "group", "classapi_models_1_1_self.html#a6f5bfc4ec732b36aae168a6fd86a9d19", null ],
    [ "phone", "classapi_models_1_1_self.html#a48f9bf4407c1e9d7a1cf11ec58420116", null ],
    [ "street_name", "classapi_models_1_1_self.html#a38d97a43ba955da0d6acc44511d7150e", null ],
    [ "street_number", "classapi_models_1_1_self.html#a5f8bb8a1545cdf797800d712040542a7", null ],
    [ "town", "classapi_models_1_1_self.html#a813b306a534f771e460370514ed1e357", null ],
    [ "user_id", "classapi_models_1_1_self.html#ad2a1772a02a485dedabfdc7831b09615", null ],
    [ "zip_code", "classapi_models_1_1_self.html#a66d686f85c1361f2c8fb9c5fa681a54e", null ]
];