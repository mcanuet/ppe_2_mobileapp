var interface_unity_editor_1_1_package_manager_1_1_u_i_1_1_i_operation_factory =
[
    [ "CreateAddOperation", "interface_unity_editor_1_1_package_manager_1_1_u_i_1_1_i_operation_factory.html#a50ed4e82d0709f835d61b47331a464ac", null ],
    [ "CreateListOperation", "interface_unity_editor_1_1_package_manager_1_1_u_i_1_1_i_operation_factory.html#a4a8dc6a8401962d62f9fe1e4eb83cde6", null ],
    [ "CreateRemoveOperation", "interface_unity_editor_1_1_package_manager_1_1_u_i_1_1_i_operation_factory.html#aa8a45874d7de57734cf4d7591b07c561", null ],
    [ "CreateSearchOperation", "interface_unity_editor_1_1_package_manager_1_1_u_i_1_1_i_operation_factory.html#a1e4b0e667c493749785f3444697e59ce", null ]
];