var class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_mock_remove_operation =
[
    [ "MockRemoveOperation", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_mock_remove_operation.html#a2e3a6f7f91c00c2820813ea1dc426718", null ],
    [ "RemovePackageAsync", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_mock_remove_operation.html#a6eb0a7297483a1f4a62cbbc890f0b843", null ],
    [ "PackageInfo", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_mock_remove_operation.html#af67f49d662205ef776d9ecfa598705d2", null ],
    [ "OnOperationError", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_mock_remove_operation.html#a4b9689b1558498c357feadef4fdef8f5", null ],
    [ "OnOperationFinalized", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_mock_remove_operation.html#a8af116b6227156dbca0b02f885a358db", null ],
    [ "OnOperationSuccess", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_mock_remove_operation.html#a456bc48236cc795d9767bb2ee52191ae", null ]
];