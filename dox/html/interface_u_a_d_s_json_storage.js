var interface_u_a_d_s_json_storage =
[
    [ "clearData", "interface_u_a_d_s_json_storage.html#aca481b6fe48e31c6c777faaf9c398ddb", null ],
    [ "deleteKey:", "interface_u_a_d_s_json_storage.html#a6bf047b98773df17c4c7204f4f163c3e", null ],
    [ "getKeys:recursive:", "interface_u_a_d_s_json_storage.html#adaa268af405a0a0a1e33ae2097e84229", null ],
    [ "getValueForKey:", "interface_u_a_d_s_json_storage.html#a48c6ab4f9032839252139402e9967a8d", null ],
    [ "hasData", "interface_u_a_d_s_json_storage.html#afe49dd5956528449bb7b00bbfa74ee89", null ],
    [ "initData", "interface_u_a_d_s_json_storage.html#a6c2891c939683064ac0f5c64c40ec4b0", null ],
    [ "set:value:", "interface_u_a_d_s_json_storage.html#a754b72e6915c791a59f0655b91106397", null ],
    [ "storageContents", "interface_u_a_d_s_json_storage.html#a579b58469c5d3f2a2d83f36f38396d8e", null ]
];