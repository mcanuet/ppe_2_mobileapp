var class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_collection_tests =
[
    [ "Constructor_Instance_FilterIsLocal", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_collection_tests.html#ae499cfbdbc1bc06ce717df7db3800bad", null ],
    [ "FetchListCache_PackagesChangeEventIsPropagated", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_collection_tests.html#ab0f1f5353902a2b2288e002845ed0336", null ],
    [ "FetchListOfflineCache_PackagesChangeEventIsPropagated", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_collection_tests.html#adac0c072dc5dbf1823dcbeafecb03749", null ],
    [ "FetchSearchCache_PackagesChangeEventIsPropagated", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_collection_tests.html#a078005e8c0ca533a653fcbfc2279f096", null ],
    [ "SetFilter_WhenFilterChange_FilterChangeEventIsPropagated", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_collection_tests.html#ad5c8cea417bd2374a2d113d3c7210d40", null ],
    [ "SetFilter_WhenFilterChange_FilterIsChanged", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_collection_tests.html#aa13b71f42838afcce7a66715458bba4a", null ],
    [ "SetFilter_WhenFilterChangeNoRefresh_PackagesChangeEventIsNotPropagated", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_collection_tests.html#a8d0665d38abfb96bb3729ee08a95539e", null ],
    [ "SetFilter_WhenNoFilterChange_FilterChangeEventIsNotPropagated", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_collection_tests.html#a74d43688a3faa108b761695644f2826f", null ],
    [ "SetFilter_WhenNoFilterChangeNoRefresh_PackagesChangeEventIsNotPropagated", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_collection_tests.html#a190bc5b3b24f4d3c72115a25e76376a3", null ],
    [ "SetFilter_WhenNoFilterChangeRefresh_PackagesChangeEventIsNotPropagated", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_collection_tests.html#ad39ad9978d0b2f64cb7860fdfdb42c36", null ],
    [ "Setup", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_collection_tests.html#a452dcbec81fffb1527a8c3e053e1ba03", null ],
    [ "TearDown", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_collection_tests.html#ab5dbc257b2e6d836808cb0e659f0f96a", null ]
];