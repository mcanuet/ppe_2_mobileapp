var searchData=
[
  ['importexamplescontentmenu',['ImportExamplesContentMenu',['../class_t_m_pro_1_1_t_m_p___package_utilities.html#aa73bd06e0ec78098db6c2d895bf8412e',1,'TMPro::TMP_PackageUtilities']]],
  ['importprojectresourcesmenu',['ImportProjectResourcesMenu',['../class_t_m_pro_1_1_t_m_p___package_utilities.html#a11f7e20b4617d90a5f1da630ed02b0dd',1,'TMPro::TMP_PackageUtilities']]],
  ['initialize_3adelegate_3a',['initialize:delegate:',['../interface_unity_ads.html#a89dc56c878efb376b6014d0ef67778a9',1,'UnityAds']]],
  ['initialize_3adelegate_3atestmode_3a',['initialize:delegate:testMode:',['../interface_unity_ads.html#a95a142d4966bb89652bfec178795e017',1,'UnityAds']]],
  ['internalcrossfadealpha',['InternalCrossFadeAlpha',['../class_t_m_pro_1_1_text_mesh_pro_u_g_u_i.html#a198b9bf0fea8a41b2c76259db7f5baff',1,'TMPro.TextMeshProUGUI.InternalCrossFadeAlpha()'],['../class_t_m_pro_1_1_t_m_p___text.html#af04bfde9fe669030330b18e59d3743cd',1,'TMPro.TMP_Text.InternalCrossFadeAlpha()']]],
  ['internalcrossfadecolor',['InternalCrossFadeColor',['../class_t_m_pro_1_1_text_mesh_pro_u_g_u_i.html#aaab93e2d40e730d419c60310f0b55605',1,'TMPro.TextMeshProUGUI.InternalCrossFadeColor()'],['../class_t_m_pro_1_1_t_m_p___text.html#a22998506360d8760ae4e7df770e0f3fb',1,'TMPro.TMP_Text.InternalCrossFadeColor()']]],
  ['isinitialized',['isInitialized',['../interface_unity_ads.html#a8698d281f4758d79a70bd20f6db4a622',1,'UnityAds']]],
  ['isready',['isReady',['../interface_unity_ads.html#ad6957c0e6ebdd2b2e5f17e97310ccd2d',1,'UnityAds']]],
  ['isready_3a',['isReady:',['../interface_unity_ads.html#ab599b1f26750aa88f4e0c7be90203276',1,'UnityAds']]],
  ['issupported',['isSupported',['../interface_unity_ads.html#a88647c57b1d0a967ac74d9f847982561',1,'UnityAds']]]
];
