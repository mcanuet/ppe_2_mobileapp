var struct_t_m_pro_1_1_color_tween =
[
    [ "ColorTweenCallback", "class_t_m_pro_1_1_color_tween_1_1_color_tween_callback.html", null ],
    [ "ColorTweenMode", "struct_t_m_pro_1_1_color_tween.html#a22465a1cc95d87abd9ca22c2dd021fbd", [
      [ "All", "struct_t_m_pro_1_1_color_tween.html#a22465a1cc95d87abd9ca22c2dd021fbdab1c94ca2fbc3e78fc30069c8d0f01680", null ],
      [ "RGB", "struct_t_m_pro_1_1_color_tween.html#a22465a1cc95d87abd9ca22c2dd021fbda889574aebacda6bfd3e534e2b49b8028", null ],
      [ "Alpha", "struct_t_m_pro_1_1_color_tween.html#a22465a1cc95d87abd9ca22c2dd021fbda6132295fcf5570fb8b0a944ef322a598", null ]
    ] ],
    [ "AddOnChangedCallback", "struct_t_m_pro_1_1_color_tween.html#a5a3af510dc44b3ad3c8973bb93197961", null ],
    [ "GetDuration", "struct_t_m_pro_1_1_color_tween.html#ad68d38055abd7079732e93a83746f30b", null ],
    [ "GetIgnoreTimescale", "struct_t_m_pro_1_1_color_tween.html#a836616b26c0730957c7a2e1d340356fb", null ],
    [ "TweenValue", "struct_t_m_pro_1_1_color_tween.html#aa2ab2acdd602a7f7c73c179d4b0b92f5", null ],
    [ "ValidTarget", "struct_t_m_pro_1_1_color_tween.html#adab4fef0982d1c37120c45baa1bf1111", null ],
    [ "duration", "struct_t_m_pro_1_1_color_tween.html#aa20e0c9773e9a8e50d0feae7c4e599a0", null ],
    [ "ignoreTimeScale", "struct_t_m_pro_1_1_color_tween.html#ad8ec0ae32214f07d8e7538636bdbdb08", null ],
    [ "startColor", "struct_t_m_pro_1_1_color_tween.html#a22d5f15683397db24c9fa7ba47e32a11", null ],
    [ "targetColor", "struct_t_m_pro_1_1_color_tween.html#af6f6e8d0db57003afbaa35865364f342", null ],
    [ "tweenMode", "struct_t_m_pro_1_1_color_tween.html#aef6eb969db69ffdcef35e0f13f2e0dc2", null ]
];