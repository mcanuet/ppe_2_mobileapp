var interface_unity_editor_1_1_package_manager_1_1_u_i_1_1_i_package_manager_extension =
[
    [ "CreateExtensionUI", "interface_unity_editor_1_1_package_manager_1_1_u_i_1_1_i_package_manager_extension.html#a32866f98b536a8863de4cfc598d8ea7d", null ],
    [ "OnPackageAddedOrUpdated", "interface_unity_editor_1_1_package_manager_1_1_u_i_1_1_i_package_manager_extension.html#a89228b54f05cae1c6b04f323c13f3563", null ],
    [ "OnPackageRemoved", "interface_unity_editor_1_1_package_manager_1_1_u_i_1_1_i_package_manager_extension.html#a9f2d1be5e21ec127b32e8138f25b38a1", null ],
    [ "OnPackageSelectionChange", "interface_unity_editor_1_1_package_manager_1_1_u_i_1_1_i_package_manager_extension.html#acc9b57b0146dac8d56f457c3a35d64a5", null ]
];