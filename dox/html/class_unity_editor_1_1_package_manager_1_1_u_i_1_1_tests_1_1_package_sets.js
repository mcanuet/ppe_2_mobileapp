var class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_sets =
[
    [ "Many", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_sets.html#a86750fa77ba04573c7eaa5d605576a6a", null ],
    [ "Many", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_sets.html#a29c323e3040bc3e0d70b26f76d72a50c", null ],
    [ "Outdated", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_sets.html#a726ae3618e67b2bda58f75ac7e6e38e2", null ],
    [ "RealPackages", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_sets.html#ad9930d9a3808998a21b66f8df56f82f1", null ],
    [ "Single", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_sets.html#a7a674621cd6993b95bf2f727f713bf62", null ],
    [ "Single", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_sets.html#ace327849f2fab008bcd128f87d94e898", null ],
    [ "TestData", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_sets.html#a17745e089c0a282188b9de778eb9a8b8", null ]
];