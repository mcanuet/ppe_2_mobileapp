var class_t_m_pro_1_1_kerning_pair =
[
    [ "KerningPair", "class_t_m_pro_1_1_kerning_pair.html#a032626cb5e1418985149eecc02e321ff", null ],
    [ "KerningPair", "class_t_m_pro_1_1_kerning_pair.html#a180b8ff824757b096d9ad32dd4655919", null ],
    [ "KerningPair", "class_t_m_pro_1_1_kerning_pair.html#aae1c4176e2099f2cb8ff1c6fb093e2bb", null ],
    [ "xOffset", "class_t_m_pro_1_1_kerning_pair.html#ab6e227878e0f55e26103c94914384617", null ],
    [ "firstGlyph", "class_t_m_pro_1_1_kerning_pair.html#af9aa2a18d3992d1fa6080e44c69fe7c5", null ],
    [ "firstGlyphAdjustments", "class_t_m_pro_1_1_kerning_pair.html#a5eed997a7f25917e7de7804a6cf052f4", null ],
    [ "secondGlyph", "class_t_m_pro_1_1_kerning_pair.html#a65feda26da1fd1a3fcdb1aa4ca681612", null ],
    [ "secondGlyphAdjustments", "class_t_m_pro_1_1_kerning_pair.html#a04339cd46e942ce4841397cb801c47d7", null ]
];