var class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_mock_operation_factory =
[
    [ "MockOperationFactory", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_mock_operation_factory.html#afbdf8caeca18b0dcb3b8669486cfde54", null ],
    [ "CreateAddOperation", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_mock_operation_factory.html#a9c4cf5b39d2c0b78864aa0609a60a21d", null ],
    [ "CreateListOperation", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_mock_operation_factory.html#a23ac4569326a62c4913e285446e6e933", null ],
    [ "CreateRemoveOperation", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_mock_operation_factory.html#a548fecbf2178df4f76eba8308584766c", null ],
    [ "CreateSearchOperation", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_mock_operation_factory.html#ac6f6ae337cc8eafb8726dcf7b697cdbe", null ],
    [ "ResetOperations", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_mock_operation_factory.html#a9f58ffa587113492f7124ae4e2974028", null ],
    [ "AddOperation", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_mock_operation_factory.html#afbe73c35242df8c6e6fa2b1a7ac2f732", null ],
    [ "Packages", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_mock_operation_factory.html#ac5b0b5d8a6b7202e5f650bbd79e94836", null ],
    [ "RemoveOperation", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_mock_operation_factory.html#a35237cef3f45a643e6faea7aa5671dc2", null ],
    [ "SearchOperation", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_mock_operation_factory.html#a69850c9fb9da5cbb687083486d539e21", null ]
];