var class_unity_editor_1_1_package_manager_1_1_u_i_1_1_package =
[
    [ "Equals", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_package.html#ab39056f1dd2aa0783f6de3f9bd42e5cc", null ],
    [ "GetHashCode", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_package.html#a595f5f2201a65d9c83dc1f86b7b230fd", null ],
    [ "Remove", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_package.html#aab70b50e6866314537b5c0a0edfd60ec", null ],
    [ "Current", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_package.html#ac9a3a0581e85149b7e0a614b9f3de3f6", null ],
    [ "IsBuiltIn", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_package.html#ace33645dd7c1185efbe59cb6ed2d0a56", null ],
    [ "IsPackageManagerUI", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_package.html#ab837ed795bd116a8f53fb33928a86c5b", null ],
    [ "Latest", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_package.html#af647790afa886350bf14e81fb59da02a", null ],
    [ "LatestPatch", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_package.html#abe3e3394cd7310123c5b4b885740d274", null ],
    [ "LatestRelease", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_package.html#a9d54b0543dc0164b8b09aa5bba7ea451", null ],
    [ "LatestUpdate", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_package.html#a0a16f0854f348cea456abe0983590de3", null ],
    [ "Name", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_package.html#a1044e19763ce3d709ee4547b975a1472", null ],
    [ "ReleaseVersions", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_package.html#a42b7b9fdf83ac5185cd6474752886876", null ],
    [ "Verified", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_package.html#a8b50d39e0d41b621aaa1b6333f28cdfe", null ],
    [ "Versions", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_package.html#ab2a7a9b00ba8e99e351d793b8010ad3b", null ],
    [ "VersionToDisplay", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_package.html#a8c7d38f44d5b6ed6ddd106b6456f7af6", null ]
];