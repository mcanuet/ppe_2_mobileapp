var struct_t_m_pro_1_1_sprite_asset_utilities_1_1_texture_packer_1_1_sprite_data =
[
    [ "filename", "struct_t_m_pro_1_1_sprite_asset_utilities_1_1_texture_packer_1_1_sprite_data.html#abe44d4712583603f32510231f847a0df", null ],
    [ "frame", "struct_t_m_pro_1_1_sprite_asset_utilities_1_1_texture_packer_1_1_sprite_data.html#aba51b783405791e2b2b3ef531c9acfc4", null ],
    [ "pivot", "struct_t_m_pro_1_1_sprite_asset_utilities_1_1_texture_packer_1_1_sprite_data.html#af1d60995dfb106ae0c1ffa3cb5175a37", null ],
    [ "rotated", "struct_t_m_pro_1_1_sprite_asset_utilities_1_1_texture_packer_1_1_sprite_data.html#af9ff19caaaa8ee148110fa3ab9b190af", null ],
    [ "sourceSize", "struct_t_m_pro_1_1_sprite_asset_utilities_1_1_texture_packer_1_1_sprite_data.html#a27fec0c459f7cccc253cb136f863e80e", null ],
    [ "spriteSourceSize", "struct_t_m_pro_1_1_sprite_asset_utilities_1_1_texture_packer_1_1_sprite_data.html#a594b760a42f7cd378fd430afbf7b54dc", null ],
    [ "trimmed", "struct_t_m_pro_1_1_sprite_asset_utilities_1_1_texture_packer_1_1_sprite_data.html#ad116f66559d6d367b914cbd3c1bb2833", null ]
];