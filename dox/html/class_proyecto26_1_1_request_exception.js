var class_proyecto26_1_1_request_exception =
[
    [ "RequestException", "class_proyecto26_1_1_request_exception.html#ae8de76bf3bc7a8f40aeba972d5e69852", null ],
    [ "RequestException", "class_proyecto26_1_1_request_exception.html#a99a9b49903d02e28c863e7e358891d13", null ],
    [ "RequestException", "class_proyecto26_1_1_request_exception.html#aa7251331e78d3269c8f38cc26b10a402", null ],
    [ "RequestException", "class_proyecto26_1_1_request_exception.html#a35c360eb15193345a8b101998ac5e1f4", null ],
    [ "IsHttpError", "class_proyecto26_1_1_request_exception.html#a516e1ae7abb8df1b402e00f2075c4ee3", null ],
    [ "IsNetworkError", "class_proyecto26_1_1_request_exception.html#a92343ae587ee1555ef3e45931b781b77", null ],
    [ "ServerMessage", "class_proyecto26_1_1_request_exception.html#a3ba95f2a3186d8e15dedde0bada5dbe9", null ],
    [ "StatusCode", "class_proyecto26_1_1_request_exception.html#a40ca502507fbb7004a816f868cd13536", null ]
];