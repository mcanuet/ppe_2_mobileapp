var class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_mock_add_operation =
[
    [ "MockAddOperation", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_mock_add_operation.html#a4a34c3a1385aae59f5a420eff9bfa696", null ],
    [ "AddPackageAsync", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_mock_add_operation.html#aab0d0a956f90a3496b0a5cba30cdd2b7", null ],
    [ "PackageInfo", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_mock_add_operation.html#af0ccee664ea462e6715d2ddc784f08e4", null ],
    [ "OnOperationError", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_mock_add_operation.html#a04c46b925ae27c871d32c763d6ae232e", null ],
    [ "OnOperationFinalized", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_mock_add_operation.html#aff278f53a4a6c41d0b35a1e0ee40397c", null ],
    [ "OnOperationSuccess", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_mock_add_operation.html#aa52928a2ce6a6df6d83b1c1572d34a30", null ]
];