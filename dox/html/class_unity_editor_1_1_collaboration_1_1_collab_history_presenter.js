var class_unity_editor_1_1_collaboration_1_1_collab_history_presenter =
[
    [ "CollabHistoryPresenter", "class_unity_editor_1_1_collaboration_1_1_collab_history_presenter.html#a98a53853b14ff55206d6d1a0c97529f1", null ],
    [ "OnUpdatePage", "class_unity_editor_1_1_collaboration_1_1_collab_history_presenter.html#adeccc84634516b7006ba0f9eeeaf42c5", null ],
    [ "OnWindowDisabled", "class_unity_editor_1_1_collaboration_1_1_collab_history_presenter.html#a1d9943c824329af4ff6008dc891b6cba", null ],
    [ "OnWindowEnabled", "class_unity_editor_1_1_collaboration_1_1_collab_history_presenter.html#acba3dc89a6ab81f606e1aab7f5f67630", null ],
    [ "ShowBuildForCommit", "class_unity_editor_1_1_collaboration_1_1_collab_history_presenter.html#af05c6fa5503d623f18938306e1c764f1", null ],
    [ "ShowServicePage", "class_unity_editor_1_1_collaboration_1_1_collab_history_presenter.html#ac2d859f1145229b9e3f79a6f3bcdadab", null ],
    [ "UpdateBuildServiceStatus", "class_unity_editor_1_1_collaboration_1_1_collab_history_presenter.html#affaaa9e1fa64b6028db288c380c0023d", null ],
    [ "ItemsPerPage", "class_unity_editor_1_1_collaboration_1_1_collab_history_presenter.html#a8c5a56abaa6379581e5d7894ea2cb65d", null ],
    [ "BuildServiceEnabled", "class_unity_editor_1_1_collaboration_1_1_collab_history_presenter.html#a8a575bcd378e10cd9f3a3c84266933fd", null ]
];