var protocol_unity_ads_delegate_p =
[
    [ "unityAdsDidError:withMessage:", "protocol_unity_ads_delegate-p.html#a2272989530c7cd577277d7f17bde3215", null ],
    [ "unityAdsDidFinish:withFinishState:", "protocol_unity_ads_delegate-p.html#a4b243b4d6d509ce4982bf8416f29dc42", null ],
    [ "unityAdsDidStart:", "protocol_unity_ads_delegate-p.html#a04f51d2948959482a526886a72a6769b", null ],
    [ "unityAdsReady:", "protocol_unity_ads_delegate-p.html#a8fc6e8a10ffb53dbc8f4c84312e4d544", null ]
];