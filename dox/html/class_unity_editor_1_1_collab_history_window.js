var class_unity_editor_1_1_collab_history_window =
[
    [ "CollabHistoryWindow", "class_unity_editor_1_1_collab_history_window.html#ae8152d2af9a699452d676ef6beb7ee8d", null ],
    [ "OnDisable", "class_unity_editor_1_1_collab_history_window.html#abc4317801a0bf1e182864cdc9c549279", null ],
    [ "OnEnable", "class_unity_editor_1_1_collab_history_window.html#a4a22a48d5e3872bbf08fc88a567d4439", null ],
    [ "SetupGUI", "class_unity_editor_1_1_collab_history_window.html#a68cea752240f1db1d6031a90061fc596", null ],
    [ "UpdateRevisions", "class_unity_editor_1_1_collab_history_window.html#a2bf80bf9b907918026d8f3337cea00e6", null ],
    [ "UpdateState", "class_unity_editor_1_1_collab_history_window.html#a4ae7622802cb319fa2e5e59d8f423ee0", null ],
    [ "inProgressRevision", "class_unity_editor_1_1_collab_history_window.html#af4770e386e2756a72cc18fa123f9a84d", null ],
    [ "itemsPerPage", "class_unity_editor_1_1_collab_history_window.html#a745e77e85001e4e18eb4ffbeea4f4be9", null ],
    [ "OnGoBackAction", "class_unity_editor_1_1_collab_history_window.html#a4029f0522b8ea6e91830cafa9fd1a7c4", null ],
    [ "OnPageChangeAction", "class_unity_editor_1_1_collab_history_window.html#a0916d14a1e99e52080e4e2f3d40a26a0", null ],
    [ "OnRestoreAction", "class_unity_editor_1_1_collab_history_window.html#a687625a3b8ed3d53ec7a892b214e6a21", null ],
    [ "OnShowBuildAction", "class_unity_editor_1_1_collab_history_window.html#ac1e29e71b4e5fefa0bdab8634a9de7fe", null ],
    [ "OnShowServicesAction", "class_unity_editor_1_1_collab_history_window.html#a384a9264f55b1d399a80cbcd0b0d33c7", null ],
    [ "OnUpdateAction", "class_unity_editor_1_1_collab_history_window.html#a75939bc67d0eca3d62b8c7664deaa523", null ],
    [ "revisionActionsEnabled", "class_unity_editor_1_1_collab_history_window.html#add615e0b09468afea675e2851124cc49", null ]
];