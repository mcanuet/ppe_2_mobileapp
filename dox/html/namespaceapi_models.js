var namespaceapi_models =
[
    [ "Car", "classapi_models_1_1_car.html", "classapi_models_1_1_car" ],
    [ "Group", "classapi_models_1_1_group.html", "classapi_models_1_1_group" ],
    [ "LoginData", "classapi_models_1_1_login_data.html", "classapi_models_1_1_login_data" ],
    [ "MakeReservation", "classapi_models_1_1_make_reservation.html", "classapi_models_1_1_make_reservation" ],
    [ "Model", "classapi_models_1_1_model.html", "classapi_models_1_1_model" ],
    [ "Reservation", "classapi_models_1_1_reservation.html", "classapi_models_1_1_reservation" ],
    [ "Right", "classapi_models_1_1_right.html", "classapi_models_1_1_right" ],
    [ "Self", "classapi_models_1_1_self.html", "classapi_models_1_1_self" ],
    [ "User", "classapi_models_1_1_user.html", "classapi_models_1_1_user" ]
];