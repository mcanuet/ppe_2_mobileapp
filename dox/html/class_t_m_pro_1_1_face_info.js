var class_t_m_pro_1_1_face_info =
[
    [ "Ascender", "class_t_m_pro_1_1_face_info.html#a24c7184e75041c3aff9b4bb8bd12e17b", null ],
    [ "AtlasHeight", "class_t_m_pro_1_1_face_info.html#ac534fec31799477b6ed641f1f52daaeb", null ],
    [ "AtlasWidth", "class_t_m_pro_1_1_face_info.html#acede0e79039968c3bdad0bc862b0c94e", null ],
    [ "Baseline", "class_t_m_pro_1_1_face_info.html#a25c912c275bb151abbf3a4fb0838e847", null ],
    [ "CapHeight", "class_t_m_pro_1_1_face_info.html#af804c415cb294feb2263fbcbfb25b6e8", null ],
    [ "CenterLine", "class_t_m_pro_1_1_face_info.html#a88a11296f260a733bd93fe6337c6bc74", null ],
    [ "CharacterCount", "class_t_m_pro_1_1_face_info.html#a625e4c7118d22fc0e9e8e930c978bd47", null ],
    [ "Descender", "class_t_m_pro_1_1_face_info.html#a96a9406c35a8c64e732fc5e427a58326", null ],
    [ "LineHeight", "class_t_m_pro_1_1_face_info.html#ac098a68d0e412e509971830c67095870", null ],
    [ "Name", "class_t_m_pro_1_1_face_info.html#a8fbfcc89c49e0f9f6b159c8fc8d1ea84", null ],
    [ "Padding", "class_t_m_pro_1_1_face_info.html#a4e9e91ce1b982e2379ade0eed9341492", null ],
    [ "PointSize", "class_t_m_pro_1_1_face_info.html#a2d120ea15c3c522b77c04901ea69ace7", null ],
    [ "Scale", "class_t_m_pro_1_1_face_info.html#aaa6254a12561da3d2005ce8c0445bc9a", null ],
    [ "strikethrough", "class_t_m_pro_1_1_face_info.html#af695879bc7d227e74aa6429459470efb", null ],
    [ "strikethroughThickness", "class_t_m_pro_1_1_face_info.html#a563ba55adbbfa14728a9ae3cd67badc3", null ],
    [ "SubscriptOffset", "class_t_m_pro_1_1_face_info.html#a87cf1904453b1f0b56e07809f2ccf248", null ],
    [ "SubSize", "class_t_m_pro_1_1_face_info.html#ad6ea70a93cce8776697ec907c5b8ede2", null ],
    [ "SuperscriptOffset", "class_t_m_pro_1_1_face_info.html#a938e5a3d334a261aba7eb0c4d571493c", null ],
    [ "TabWidth", "class_t_m_pro_1_1_face_info.html#a05d6a8a6e72fceefc77d70a61b3ecf8c", null ],
    [ "Underline", "class_t_m_pro_1_1_face_info.html#ac84141f34b2d75aa4bac5170c94a2970", null ],
    [ "UnderlineThickness", "class_t_m_pro_1_1_face_info.html#a0dd686bb840ba17903f59848268af48c", null ]
];