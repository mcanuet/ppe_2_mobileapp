var class_unity_editor_1_1_collaboration_1_1_tests_1_1_test_history_window =
[
    [ "UpdateRevisions", "class_unity_editor_1_1_collaboration_1_1_tests_1_1_test_history_window.html#a03d65448ca22f7bf170d4ef0673b48cc", null ],
    [ "UpdateState", "class_unity_editor_1_1_collaboration_1_1_tests_1_1_test_history_window.html#ae36c3e34b7ea55f9142251ff4fc0a7db", null ],
    [ "items", "class_unity_editor_1_1_collaboration_1_1_tests_1_1_test_history_window.html#af348109cc3edbf923cae66a0482e3474", null ],
    [ "errMessage", "class_unity_editor_1_1_collaboration_1_1_tests_1_1_test_history_window.html#a4e9d369ea53f55fd8f27283b876202c1", null ],
    [ "inProgressRevision", "class_unity_editor_1_1_collaboration_1_1_tests_1_1_test_history_window.html#a82fc71893d8afab27c74b590851cf7f9", null ],
    [ "itemsPerPage", "class_unity_editor_1_1_collaboration_1_1_tests_1_1_test_history_window.html#a9d04fd55f3a51e9792e8d158caefa801", null ],
    [ "OnGoBackAction", "class_unity_editor_1_1_collaboration_1_1_tests_1_1_test_history_window.html#afb9c488cb012687d02307ad926c872ea", null ],
    [ "OnPageChangeAction", "class_unity_editor_1_1_collaboration_1_1_tests_1_1_test_history_window.html#a66823c1ff04b14a4a124d26a9819ec43", null ],
    [ "OnRestoreAction", "class_unity_editor_1_1_collaboration_1_1_tests_1_1_test_history_window.html#a3f23b3e6a7ece762b19df21bd8d0cc9d", null ],
    [ "OnShowBuildAction", "class_unity_editor_1_1_collaboration_1_1_tests_1_1_test_history_window.html#aba91eb3ffb5ed2d32099f0dee4df6ecb", null ],
    [ "OnShowServicesAction", "class_unity_editor_1_1_collaboration_1_1_tests_1_1_test_history_window.html#a184442eb092bb9831f82cf1f02e9685e", null ],
    [ "OnUpdateAction", "class_unity_editor_1_1_collaboration_1_1_tests_1_1_test_history_window.html#ac10eb4e6d99658a40c08ef1570398033", null ],
    [ "revisionActionsEnabled", "class_unity_editor_1_1_collaboration_1_1_tests_1_1_test_history_window.html#a67ba9048c478720cc6a8c0add533232a", null ]
];