var namespace_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests =
[
    [ "MockAddOperation", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_mock_add_operation.html", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_mock_add_operation" ],
    [ "MockListOperation", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_mock_list_operation.html", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_mock_list_operation" ],
    [ "MockOperation", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_mock_operation.html", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_mock_operation" ],
    [ "MockOperationFactory", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_mock_operation_factory.html", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_mock_operation_factory" ],
    [ "MockRemoveOperation", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_mock_remove_operation.html", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_mock_remove_operation" ],
    [ "MockSearchOperation", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_mock_search_operation.html", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_mock_search_operation" ],
    [ "PackageBaseTests", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_base_tests.html", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_base_tests" ],
    [ "PackageCollectionTests", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_collection_tests.html", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_collection_tests" ],
    [ "PackageDetailsTests", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_details_tests.html", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_details_tests" ],
    [ "PackageInfoTests", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_info_tests.html", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_info_tests" ],
    [ "PackageManagerWindowTests", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_manager_window_tests.html", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_manager_window_tests" ],
    [ "PackageSearchTests", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_search_tests.html", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_search_tests" ],
    [ "PackageSets", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_sets.html", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_sets" ],
    [ "PackageTests", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_tests.html", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_tests" ],
    [ "UITests", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_u_i_tests.html", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_u_i_tests" ]
];