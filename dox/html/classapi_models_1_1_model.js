var classapi_models_1_1_model =
[
    [ "brand", "classapi_models_1_1_model.html#aec90622ab0993c94cc4f7cc0b77ca496", null ],
    [ "category", "classapi_models_1_1_model.html#aa827a8d5372a078865226365afb82947", null ],
    [ "engine", "classapi_models_1_1_model.html#ab3edc3255867f16f8e7432d256b50660", null ],
    [ "fuel_type", "classapi_models_1_1_model.html#a4cd5a69d8bb44268efdab8d7306e7b2f", null ],
    [ "id", "classapi_models_1_1_model.html#a4d0599af9244b6ecfeb6a516dda58b3f", null ],
    [ "image", "classapi_models_1_1_model.html#abd85184c3b42f2406c7aaee49477391a", null ],
    [ "model", "classapi_models_1_1_model.html#a773e90c3e424702ad5bca686148b927b", null ],
    [ "places", "classapi_models_1_1_model.html#a214ec27a37feedfe47b615355a927a59", null ]
];