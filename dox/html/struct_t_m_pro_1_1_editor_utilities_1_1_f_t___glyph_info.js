var struct_t_m_pro_1_1_editor_utilities_1_1_f_t___glyph_info =
[
    [ "height", "struct_t_m_pro_1_1_editor_utilities_1_1_f_t___glyph_info.html#a38151b6079db1207d93f4cb00f9912d9", null ],
    [ "id", "struct_t_m_pro_1_1_editor_utilities_1_1_f_t___glyph_info.html#a4d16845221c8b09aaadcc81acbab48c8", null ],
    [ "width", "struct_t_m_pro_1_1_editor_utilities_1_1_f_t___glyph_info.html#a9cc104a777aeffc86925976a5c3debad", null ],
    [ "x", "struct_t_m_pro_1_1_editor_utilities_1_1_f_t___glyph_info.html#a89d73dfe840bb743a58cf036aae570b8", null ],
    [ "xAdvance", "struct_t_m_pro_1_1_editor_utilities_1_1_f_t___glyph_info.html#adcc69a9fb9d88bf52f811573db7347ce", null ],
    [ "xOffset", "struct_t_m_pro_1_1_editor_utilities_1_1_f_t___glyph_info.html#a345fbe6d87887b14de6fc161e54f6b5b", null ],
    [ "y", "struct_t_m_pro_1_1_editor_utilities_1_1_f_t___glyph_info.html#ab4a54684e42307ba57fba2bf58300372", null ],
    [ "yOffset", "struct_t_m_pro_1_1_editor_utilities_1_1_f_t___glyph_info.html#afdf4f2b7cf3e2b840811d11ff3533181", null ]
];