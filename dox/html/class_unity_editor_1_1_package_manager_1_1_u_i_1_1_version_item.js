var class_unity_editor_1_1_package_manager_1_1_u_i_1_1_version_item =
[
    [ "Equals", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_version_item.html#af3bf093bafcabc8fa80a01e1b4f92068", null ],
    [ "GetHashCode", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_version_item.html#a6db7faf624ece352e3992e43b807164f", null ],
    [ "ToString", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_version_item.html#a492dc1d703e1ef977bf9ab7748eb9e04", null ],
    [ "DropdownLabel", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_version_item.html#a172dc27962da262e4b0874381e399d3d", null ],
    [ "Label", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_version_item.html#a80389f773561decd881c144a418def8f", null ],
    [ "MenuName", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_version_item.html#ae07b13c5b5c642e61f1d06c2370d0158", null ]
];