var hierarchy =
[
    [ "apiModels.Car", "classapi_models_1_1_car.html", null ],
    [ "Exception", null, [
      [ "Proyecto26.RequestException", "class_proyecto26_1_1_request_exception.html", null ]
    ] ],
    [ "apiModels.Group", "classapi_models_1_1_group.html", null ],
    [ "IObjectRecoEventHandler", null, [
      [ "DefaultModelRecoEventHandler", "class_default_model_reco_event_handler.html", null ]
    ] ],
    [ "ITrackableEventHandler", null, [
      [ "DefaultTrackableEventHandler", "class_default_trackable_event_handler.html", null ]
    ] ],
    [ "apiModels.LoginData", "classapi_models_1_1_login_data.html", null ],
    [ "apiModels.MakeReservation", "classapi_models_1_1_make_reservation.html", null ],
    [ "apiModels.Model", "classapi_models_1_1_model.html", null ],
    [ "MonoBehaviour", null, [
      [ "BoundingBoxRenderer", "class_bounding_box_renderer.html", null ],
      [ "dasboard", "classdasboard.html", null ],
      [ "DefaultModelRecoEventHandler", "class_default_model_reco_event_handler.html", null ],
      [ "DefaultTrackableEventHandler", "class_default_trackable_event_handler.html", null ],
      [ "Login", "class_login.html", null ],
      [ "makeRes", "classmake_res.html", null ],
      [ "showRes", "classshow_res.html", null ],
      [ "viewModel", "classview_model.html", null ]
    ] ],
    [ "Proyecto26.RequestHelper", "class_proyecto26_1_1_request_helper.html", null ],
    [ "apiModels.Reservation", "classapi_models_1_1_reservation.html", null ],
    [ "Proyecto26.ResponseHelper", "class_proyecto26_1_1_response_helper.html", null ],
    [ "apiModels.Right", "classapi_models_1_1_right.html", null ],
    [ "apiModels.Self", "classapi_models_1_1_self.html", null ],
    [ "apiModels.User", "classapi_models_1_1_user.html", null ],
    [ "VuforiaMonoBehaviour", null, [
      [ "DefaultInitializationErrorHandler", "class_default_initialization_error_handler.html", null ]
    ] ]
];