var class_unity_editor_1_1_package_manager_1_1_u_i_1_1_package_item =
[
    [ "PackageItem", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_package_item.html#a3baa5052f2cb0a2245b45a18caa9bec8", null ],
    [ "PackageItem", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_package_item.html#ac98665dc507d261f7fae6dced19b7108", null ],
    [ "SetSelected", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_package_item.html#a561285835646beec4fcd677a2a06278b", null ],
    [ "nextItem", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_package_item.html#a7901cff10504efa742331aed2f87fcc1", null ],
    [ "packageGroup", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_package_item.html#a9705f50ae79bd941ff282c47a97bec2b", null ],
    [ "previousItem", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_package_item.html#a195630cf7aa560a6bf6573fa6c86fbe0", null ],
    [ "NameLabel", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_package_item.html#aef5260cee9caf0501e41662f069b7c62", null ],
    [ "package", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_package_item.html#ae1fd38fe3b548b78ce8dd5b24d73d784", null ],
    [ "PackageContainer", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_package_item.html#a71cd0488ac08ec9486d6be4259f05a49", null ],
    [ "Spinner", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_package_item.html#a7e12bd6d31c32d824cf3b4e4bb135f20", null ],
    [ "StateLabel", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_package_item.html#affe04879371fe2a3702f6153a6831b1c", null ],
    [ "VersionLabel", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_package_item.html#ac2fffb8d53d0576159eb2c14b21af5d0", null ],
    [ "OnSelected", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_package_item.html#ada9c99405a7245f7d23fe5de001ac602", null ]
];