var class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_details_tests =
[
    [ "Setup", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_details_tests.html#a59e25fd7a6aab61a6aea2e0ae8964f61", null ],
    [ "Show_CorrectLabel_Git", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_details_tests.html#a61b724f3056f6ae44b9283bd546c9329", null ],
    [ "Show_CorrectLabel_Install", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_details_tests.html#a331d5bcc5b96e2c913b094756efce0b4", null ],
    [ "Show_CorrectLabel_LocalFolder", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_details_tests.html#a37997d39e800c9658e978f7a40940759", null ],
    [ "Show_CorrectLabel_UpdateTo", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_details_tests.html#a2ac632f640db1c66f02bd2b3e621c98d", null ],
    [ "Show_CorrectLabel_UpToDate", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_details_tests.html#af059d4f10a91b86b022adec6b2a8458a", null ],
    [ "Show_CorrectTag", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_details_tests.html#a596c8c89e303ba583b51c1dbd53ef9cd", null ],
    [ "Show_HideLabel_Embedded", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_details_tests.html#afc925f444710c88e642b906ed82e0363", null ]
];