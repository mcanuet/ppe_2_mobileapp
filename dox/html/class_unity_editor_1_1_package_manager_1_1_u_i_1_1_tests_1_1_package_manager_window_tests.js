var class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_manager_window_tests =
[
    [ "ListPackages_UsesCache", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_manager_window_tests.html#a046446c91e9836804c13619064db4f77", null ],
    [ "SearchPackages_UsesCache", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_manager_window_tests.html#aab7c5110bc86eb18b1f4ef9da267ce59", null ],
    [ "Setup", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_manager_window_tests.html#ad0ad24cd1a0baf37359c9e1b6737fcd0", null ],
    [ "TearDown", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_manager_window_tests.html#ad620512957ff2f08d758b2e5e98a1b99", null ],
    [ "When_Default_FirstPackageUIElement_HasSelectedClass", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_manager_window_tests.html#a245beeb834f41f5fe9cd79d5f7e6910f", null ],
    [ "When_Filter_Changes_Shows_Correct_List", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_manager_window_tests.html#ad2459ae2e6a2270c6dc40bc69d98502a", null ],
    [ "When_PackageCollection_Remove_Fails_PackageLists_NotUpdated", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_manager_window_tests.html#a43491762dcc44e54c39bf1e83923a320", null ],
    [ "When_PackageCollection_Remove_PackageLists_Updated", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_manager_window_tests.html#a818e9d20a154f1b47ed7a3cfca9375c9", null ],
    [ "When_PackageCollection_Update_Fails_Package_Stay_Current", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_manager_window_tests.html#abc176b6b86e14738c960b6ce51742a1a", null ],
    [ "When_PackageCollection_Updates_PackageList_Updates", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_package_manager_window_tests.html#a068dabc7b2d879a8c66c70395b38db3d", null ]
];