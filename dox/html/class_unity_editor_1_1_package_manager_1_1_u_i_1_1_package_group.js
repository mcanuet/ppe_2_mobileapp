var class_unity_editor_1_1_package_manager_1_1_u_i_1_1_package_group =
[
    [ "PackageGroup", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_package_group.html#a3940dfe9681ca82a6c357becf6d8718f", null ],
    [ "PackageGroup", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_package_group.html#a84e44c2d92af75a10b1be79c806ca55a", null ],
    [ "firstPackage", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_package_group.html#a24d18090b8f460302bcf5353f6d62b58", null ],
    [ "lastPackage", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_package_group.html#a15e617e368bf709ff93c9ff26e9d093a", null ],
    [ "nextGroup", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_package_group.html#ad4211356a171a288c8c831b10c68510d", null ],
    [ "previousGroup", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_package_group.html#a17673340659d03fe29324de988ebc2d6", null ],
    [ "HeaderTitle", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_package_group.html#a53fd1b187311bdd100a4a618828e0926", null ],
    [ "List", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_package_group.html#ad214cbf72024bc91a10103ccf0783c95", null ]
];