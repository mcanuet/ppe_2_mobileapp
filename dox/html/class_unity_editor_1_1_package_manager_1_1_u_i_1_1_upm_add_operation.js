var class_unity_editor_1_1_package_manager_1_1_u_i_1_1_upm_add_operation =
[
    [ "AddPackageAsync", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_upm_add_operation.html#ae93614e9b995f7c8fcf0456c38be1cdd", null ],
    [ "CreateRequest", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_upm_add_operation.html#a5b4cbf090d82606a14caf9fd90ccd0c0", null ],
    [ "ProcessData", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_upm_add_operation.html#afcf098692ff87d8f0b657a5c52e165b2", null ],
    [ "PackageInfo", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_upm_add_operation.html#a9ea676c65a0212e075b541ee3fd64c70", null ],
    [ "OnOperationSuccess", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_upm_add_operation.html#a291a66dd365a5e5afc3ab716b8e94288", null ]
];