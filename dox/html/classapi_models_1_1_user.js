var classapi_models_1_1_user =
[
    [ "email", "classapi_models_1_1_user.html#a1157911a748b24a1f8e85ee04babdf4a", null ],
    [ "group", "classapi_models_1_1_user.html#a637339839dceac2c8b665a640d667eb4", null ],
    [ "id", "classapi_models_1_1_user.html#a72bfb8b370a00c087bf4db1510a85380", null ],
    [ "password", "classapi_models_1_1_user.html#a1f1bc25987058d3795414048ea177293", null ],
    [ "phone", "classapi_models_1_1_user.html#ae3bd0369130d9919df52e5fa906e4dbb", null ],
    [ "street_name", "classapi_models_1_1_user.html#accace41856a5aa9ac90066e33f51fd01", null ],
    [ "street_number", "classapi_models_1_1_user.html#a2b40d76017c0b9b1636d9e5985b29ddb", null ],
    [ "town", "classapi_models_1_1_user.html#a8995d95c8b602714887a4a364d382424", null ],
    [ "zip_code", "classapi_models_1_1_user.html#a61385595119f4ef415a0a4d984a37c10", null ]
];