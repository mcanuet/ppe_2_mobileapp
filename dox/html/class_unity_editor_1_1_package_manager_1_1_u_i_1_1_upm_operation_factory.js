var class_unity_editor_1_1_package_manager_1_1_u_i_1_1_upm_operation_factory =
[
    [ "CreateAddOperation", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_upm_operation_factory.html#a567651d9dbfbb83813ae45768ab31e4e", null ],
    [ "CreateListOperation", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_upm_operation_factory.html#ac7d8532219c1ee8dbb59a15175d5944a", null ],
    [ "CreateRemoveOperation", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_upm_operation_factory.html#ab996d83fd2c343c982c264fdf046ab90", null ],
    [ "CreateSearchOperation", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_upm_operation_factory.html#a49788149061bc550315ab55adb0a89d8", null ]
];