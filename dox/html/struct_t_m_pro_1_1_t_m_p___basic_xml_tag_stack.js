var struct_t_m_pro_1_1_t_m_p___basic_xml_tag_stack =
[
    [ "Add", "struct_t_m_pro_1_1_t_m_p___basic_xml_tag_stack.html#a783f33bfcbeaa7cd8ca00fa7f90c8b37", null ],
    [ "Clear", "struct_t_m_pro_1_1_t_m_p___basic_xml_tag_stack.html#a67f44a1d30eec477ca2b1ef66c5a3635", null ],
    [ "Remove", "struct_t_m_pro_1_1_t_m_p___basic_xml_tag_stack.html#a437fa11f63cde0b26db36285d85e1b5c", null ],
    [ "bold", "struct_t_m_pro_1_1_t_m_p___basic_xml_tag_stack.html#a4a430a9102bd1e8a2125297319a2cd11", null ],
    [ "highlight", "struct_t_m_pro_1_1_t_m_p___basic_xml_tag_stack.html#a0255c2e005328c8c98ecdb1a84a6b15a", null ],
    [ "italic", "struct_t_m_pro_1_1_t_m_p___basic_xml_tag_stack.html#a7a4b3a0dbe3b8a3919fdb95497bf0a1f", null ],
    [ "lowercase", "struct_t_m_pro_1_1_t_m_p___basic_xml_tag_stack.html#a389292686fb1ba06bfe48c50e9057185", null ],
    [ "smallcaps", "struct_t_m_pro_1_1_t_m_p___basic_xml_tag_stack.html#a1902e27b99936d5625b338540d1d5884", null ],
    [ "strikethrough", "struct_t_m_pro_1_1_t_m_p___basic_xml_tag_stack.html#a9137f5f5200ce9b4ce94176b46508eb3", null ],
    [ "subscript", "struct_t_m_pro_1_1_t_m_p___basic_xml_tag_stack.html#a09d330494aa5aefda65daf8f02c9d98a", null ],
    [ "superscript", "struct_t_m_pro_1_1_t_m_p___basic_xml_tag_stack.html#a6e883a6d5ae1594d0e748eb3ee65fa07", null ],
    [ "underline", "struct_t_m_pro_1_1_t_m_p___basic_xml_tag_stack.html#a6719da4dddf675308cd077d70843a1e7", null ],
    [ "uppercase", "struct_t_m_pro_1_1_t_m_p___basic_xml_tag_stack.html#a207b4f8ec865f6a09e3c176e207df869", null ]
];