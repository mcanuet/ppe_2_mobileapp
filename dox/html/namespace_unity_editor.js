var namespace_unity_editor =
[
    [ "Collaboration", "namespace_unity_editor_1_1_collaboration.html", "namespace_unity_editor_1_1_collaboration" ],
    [ "PackageManager", "namespace_unity_editor_1_1_package_manager.html", "namespace_unity_editor_1_1_package_manager" ],
    [ "CollabHistoryWindow", "class_unity_editor_1_1_collab_history_window.html", "class_unity_editor_1_1_collab_history_window" ],
    [ "CollabToolbarButton", "class_unity_editor_1_1_collab_toolbar_button.html", "class_unity_editor_1_1_collab_toolbar_button" ],
    [ "CollabToolbarWindow", "class_unity_editor_1_1_collab_toolbar_window.html", "class_unity_editor_1_1_collab_toolbar_window" ],
    [ "WebViewStatic", "class_unity_editor_1_1_web_view_static.html", null ]
];