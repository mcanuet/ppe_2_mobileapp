var class_unity_editor_1_1_package_manager_1_1_u_i_1_1_package_collection =
[
    [ "AddPackageError", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_package_collection.html#a8bd5c0f86d4add56e3e7b578ed70e4a5", null ],
    [ "GetPackageByName", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_package_collection.html#a14d75c5f573cc182d33fe76cd4f3d7e8", null ],
    [ "GetPackageError", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_package_collection.html#a05751206796fd79618b248930a0711ea", null ],
    [ "RemovePackageErrors", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_package_collection.html#a675bfec6785ec51c5f8c2d1d2aab3ff0", null ],
    [ "SetFilter", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_package_collection.html#a376f256e424d8ac231a101e35d83799b", null ],
    [ "UpdatePackageCollection", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_package_collection.html#a96dc5d2b0f975f54efa2a57a93d0cbdb", null ],
    [ "ListSignal", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_package_collection.html#a6859e240ee04e416808e527b11107775", null ],
    [ "SearchSignal", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_package_collection.html#a0ae7851281d0246d8c715db1e3b10a63", null ],
    [ "Filter", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_package_collection.html#ae9d3d53d670427dfbe398441d7be141e", null ],
    [ "LatestListPackages", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_package_collection.html#aecaff6ca4f7866ee058b7c70d3aa49bc", null ],
    [ "LatestSearchPackages", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_package_collection.html#a38b81a3076cdebddbfb0e4e09c0e807f", null ],
    [ "SelectedPackage", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_package_collection.html#a1016b6f0d5db71f12b7e3948e8707841", null ],
    [ "OnFilterChanged", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_package_collection.html#afdd6859364e3a88ba00e9a3d9e092c09", null ],
    [ "OnPackagesChanged", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_package_collection.html#a41622c71bb27ca9e063a73e09dffb382", null ]
];