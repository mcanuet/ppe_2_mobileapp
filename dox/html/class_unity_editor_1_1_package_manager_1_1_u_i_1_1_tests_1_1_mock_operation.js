var class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_mock_operation =
[
    [ "MockOperation", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_mock_operation.html#a9cabeb25fa9d36f75e59012f142583dd", null ],
    [ "Cancel", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_mock_operation.html#a8d5c37459e639081a1812925336e2756", null ],
    [ "Factory", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_mock_operation.html#a2fda81e2896b7efe70101dad2565bd9c", null ],
    [ "ForceError", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_mock_operation.html#a3796db44c589af7e5fdb784c49b5d99d", null ],
    [ "IsCompleted", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_mock_operation.html#a9602c2b82e8315eada49ac24c117ec84", null ],
    [ "OnOperationError", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_mock_operation.html#a943d20308cda1acecdff657c48deb668", null ],
    [ "OnOperationFailure", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_mock_operation.html#a9aab252c1a0a7319315d8cf523819903", null ],
    [ "OnOperationFinalized", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_mock_operation.html#af503f49be73f10affc86658cde2cc94c", null ],
    [ "RequireNetwork", "class_unity_editor_1_1_package_manager_1_1_u_i_1_1_tests_1_1_mock_operation.html#ae03c99af48f81df0393929d733098bca", null ]
];