var class_unity_editor_1_1_collab_toolbar_button =
[
    [ "CollabToolbarButton", "class_unity_editor_1_1_collab_toolbar_button.html#a6495491ce0577c6e269320479c1cd39e", null ],
    [ "Dispose", "class_unity_editor_1_1_collab_toolbar_button.html#a09ef0c4fd1ad2688bba4a07197c21e05", null ],
    [ "OnCollabStateChanged", "class_unity_editor_1_1_collab_toolbar_button.html#afc002429a2ef73b47d0f54972333537b", null ],
    [ "OnGUI", "class_unity_editor_1_1_collab_toolbar_button.html#aa3c538550fba2437f689458095cc5979", null ],
    [ "UpdateCollabToolbarState", "class_unity_editor_1_1_collab_toolbar_button.html#ac0b3213298e3dead1742c10e9b65f5f9", null ],
    [ "currentCollabContent", "class_unity_editor_1_1_collab_toolbar_button.html#a216c2849daf4540274861da038e1fe7a", null ]
];